### "A short workshop in R"

#### Jornadas de Bioinformática (3ª Edição)

![Logo EST](assets/logo-ESTB.png)

<center>Francisco Pina Martins</center>

<center>[@FPinaMartins](https://twitter.com/FPinaMartins)</center>

<center>09/06/2020</center>

---

## An intro to "GNU R"

<img src="assets/R_logo.png" style="background:none; border:none; box-shadow:none;">

[GNU R](https://www.r-project.org/)

---

### Meet R

<img src="assets/R-console.gif" style="background:none; border:none; box-shadow:none;">

---

### RStudio - an R IDE

![RStudio_image](assets/rstudio.png)

* Text editor (Top left) <!-- .element: class="fragment" data-fragment-index="1" -->
* Console (Bottom left) <!-- .element: class="fragment" data-fragment-index="2" -->
* Workspace objects (Top right) <!-- .element: class="fragment" data-fragment-index="3" -->
* Multi-purpose (Bottom right) <!-- .element: class="fragment" data-fragment-index="4" -->

---

### How do I use this thing?

#### Calculator

<pre class="fragment"><code>4 + 4
[1] 8

(2 + 5) * 6 / (6 - 2)
[1] 10.5
</code></pre>

|||


### How do I use this thing?

#### Text processor

<pre class="fragment"><code>print("Good Morning World!")
[1] "Good Morning World!"
</code></pre>

<img class="fragment" src="assets/BS_good_morning.gif" style="background:none; border:none; box-shadow:none;">

|||

### How do I use this thing?

```R
2 < 3  # Logical operators

2 > 3

2 <= 3

2 >= 3

2 == 2

2 == 2 | 2 >= 3  # OR

2 == 2 & 2 < 3  # AND
```

---

### R programming 101

#### Variable assignment

```R
txt = "Good Morning World!"
txt <- "Good Morning World!"  # Old syntax
print(txt)
```

---

### "Simple" variable types

* Numeric (1.5) - AKA "Float" <!-- .element: class="fragment" data-fragment-index="1" -->
* Integer (1) - these have to be coerced <!-- .element: class="fragment" data-fragment-index="2" -->
* Complex (1+2i) - for imaginary numbers <!-- .element: class="fragment" data-fragment-index="3" -->
* Logical (TRUE) - or FALSE <!-- .element: class="fragment" data-fragment-index="4" -->
* Character ("ABC") - AKA "String" <!-- .element: class="fragment" data-fragment-index="5" -->

|||

### "Simple" variable types

```
number = 1.5
class(number)
```

* Try this with other variable types <!-- .element: class="fragment" data-fragment-index="1" -->

[Would you like to know more?](https://stackoverflow.com/questions/35445112/what-is-the-difference-between-mode-and-class-in-r) <!-- .element: class="fragment" data-fragment-index="2" -->

---

### "Multiple" variable types

#### Vector

A **vector** is a **sequence** of data elements **of the same basic type**. Members in a vector are called "components". It is defined using `c()`:

```R
c(1,2,1)

c("AA", "Aa", "aa")

c(TRUE, FALSE, TRUE)
```

|||

### Vector

We can perform a lot of operations on vectors:

```R
a = c(1, 3, 5, 7)
a * 5  # Multiplication
b = c(1, 2, 4, 8) 
a + b  # Arithmetics with vectors
d = c(1, 2)
a + d  # Recycling rule!
a[2]  # Indexing
```

---

### "Multiple" variable types

#### Factor

Factors are used for storing **categorical variables**.

<img src="assets/pokeball.png" style="background:none; border:none; box-shadow:none;">

<pre class="fragment"><code>captured_pokemon = c("normal", "normal", "electric", "fire", "fire", "fire", "water", "grass")
captured_pokemon_types = factor(captured_pokemon)
</pre></code>

---

### "Multiple" variable types

#### Matrix

A **matrix** is a collection of **data elements** arranged in a **two-dimensional rectangular layout**. It is defined using a `function`.

```R
M = matrix(c(1, 2, 3, 4, 5, 6),  # data elements
           nrow=2,  # number of rows
           ncol=3)  # number of columns
M

M[1,2]  # Bidimensional indexing!
M[1,]  # Get a single row
M[,1]  # Get a single column
```

---

### "Multiple" variable types

#### List

A list is a generic vector containing other objects

```R
n = c(2, 3, 5) 
s = c("aa", "bb", "cc", "dd", "ee") 
b = c(TRUE, FALSE, TRUE, FALSE, FALSE) 
x = list(n, s, b, 3)  # x contains copies of n, s, b 
```

---

### "Multiple" variable types

#### Data Frame

A **data frame** is used for storing data tables. It is a set of vectors of equal length.

```R
n = c(2, 3, 5) 
s = c("aa", "bb", "cc") 
b = c(TRUE, FALSE, TRUE) 
dframe = data.frame(n, s, b)  # dframe is a data frame 
```

|||

### "Multiple" variable types

#### Data Frame

```R
mtcars  # Built in data! 
head(mtcars)  # Try this instead...
mtcars[1, 2]  # Indexing
mtcars["Mazda RX4", "cyl"]  # Named indexing!

mtcars[[9]]  # Get column 9
mtcars[,9]  # Also get column 9
mtcars[["am"]]  # Get column "am"
mtcars$am  # Alternative - notice the lack of quotes
mtcars[,"am"]  # Another alternative
mtcars["mpg"]  # Calling it like this will get the names too

mtcars[1,]  # Get row 1

mtcars$am == 0  # Logical indexing!!!111!!one

mtcars[mtcars$cyl == 6,]  # Can you guess what this does?
```

---

### Functions

R functions are invoked by its **name**, then followed by the parenthesis, and zero or more arguments.

```R
c(1, 2, 3)

sum(1, 2)

head(mtcars)

seq(10, 30)
```

* We can also define our own functions. <!-- .element: class="fragment" data-fragment-index="1" -->

<img src="assets/not_this_day.jpg" style="background:none; border:none; box-shadow:none;"> <!-- .element: class="fragment" data-fragment-index="2" -->


|||

### Functions

```R
# See also

mean()
sd()
max()
min()
summary()
table()

```

---

### Variable coercion

* There are functions than can be used to transform variables:

```R

as.data.frame(MATRIX)

as.numeric(INTEGER)

as.character(NUMERIC)

# Many more!

```

---

### Getting help

"When in doubt, ask."
You can ask R for help with the command `?function`. Or if you need help on a **topic** rather than a function, use the `??` notation ("fuzzy search").

```R
?head

??multivariate
```

---

### Installing external packages

<ul>
  <li class="fragment">
  One of R's greatest strengths is how many 3rd party packages are developed for it
  </li>
  <li class="fragment">
  Installing these external packages is as easy as typing `install.packages(pkg_name)`
  </li>
  <li class="fragment">
  We then have to load the packages we need in order to use them
  </li>
</ul>

<pre class="fragment">
<code>install.packages("psych")

library("psych")
</code></pre>

---

### Missing data

Missing data is coded in R as `NA`, which stands for "**N**ot **A**vailable"

```R
v = c(1, 2, NA, 4)
is.na(v)

mean(v)

mean(v, na.rm=TRUE)
```

---

### Loading data from external sources

You don't have to type data into R manually. You can load data directly from files. You can use `read.csv()` or `read.table()`.

```R
data = read.csv("/path/to/file.csv")

data = read.table("/path/to/file.txt", header=FALSE, sep="\t")  # Harder to use alternative

data = read.csv("https://some.site.with.data.com/datafile.txt", header=TRUE, sep=";", dec=",")
```

---

### 5' Break

---

### Exploratory data analysis

### PCA <!-- .element: class="fragment" data-fragment-index="1" -->

---

### What is a PCA?

* "**P**rincipal **C**omponent **A**nalysis"
* An exploratory method <!-- .element: class="fragment" data-fragment-index="1" -->
* A dimensional reduction technique <!-- .element: class="fragment" data-fragment-index="2" -->
* A way to cluster data from large, complex, datasets <!-- .element: class="fragment" data-fragment-index="3" -->

---

### When is a PCA useful?

* If data simplification is required
* If variables are highly correlated <!-- .element: class="fragment" data-fragment-index="1" -->
* If dealing with 3 dimensional or higher data <!-- .element: class="fragment" data-fragment-index="2" -->
* As a starting point for other analyses <!-- .element: class="fragment" data-fragment-index="3" -->

---

### How does a PCA 'work'?

* Uses orthogonal transformation
* Converts possibly correlated variables into uncorrelated <!-- .element: class="fragment" data-fragment-index="1" -->
* The new variables are called "principal components" (PCs) <!-- .element: class="fragment" data-fragment-index="2" -->
* The PCs are sorted by the amount of variability they explain <!-- .element: class="fragment" data-fragment-index="3" -->

[Would you like to know more?](https://tgmstat.wordpress.com/2013/11/21/introduction-to-principal-component-analysis-pca/) <!-- .element: class="fragment" data-fragment-index="4" -->

---

### A simple example

* Suppose we have some students and their respective grades
* We need to group students based on their grades <!-- .element: class="fragment" data-fragment-index="1" -->

|||

### Showtime!

Let's try to group some students.
Get the data and take a look at what we have obtained.

```R
student_df = read.csv("https://stuntspt.gitlab.io/r_workshop_2020/assets/students.csv",
                      header=TRUE,
                      row.names=1,
                      sep=";")
View(student_df)
```

|||

### Showtime!

Using the "Universe" as the discriminant

```R
# Calculate the PCA
studentPCA = prcomp(student_df[,1:4])  # Why not the entire DF?

# Define colours for the "Universe" grouping
universe_colours = as.numeric(factor(student_df[,"Universe"],
                                     levels=unique(student_df[,"Universe"])))

# Draw the plot
plot(studentPCA$x[,1:2], main="PCA student plot", col=universe_colours)

# Optionally label each point
text(studentPCA$x[,1], studentPCA$x[,2], rownames(student_df), pos= 3 )

# Draw a nice looking legend
legend("bottomright", legend=unique(student_df[, "Universe"]), pch = 1,
       col=unique(universe_colours))

```

|||

### Showtime!

Using the "Category" as the discriminant

```R
# Next, do the same, but for another discriminant
category_colours = as.numeric(factor(student_df[,"Category"],
                                  levels=unique(student_df[,"Category"])))

plot(studentPCA$x[,1:2], main="PCA student plot", col=category_colours)
text(studentPCA$x[,1], studentPCA$x[,2], rownames(student_df), pos= 3 )

legend("bottomright", legend=unique(student_df[, "Category"]), pch = 1,
       col=unique(category_colours))
```

---

### Let's scale things up a bit...

```R
# Get some data
wine <- read.csv("http://archive.ics.uci.edu/ml/machine-learning-databases/wine/wine.data", sep=",")

# Add column names
colnames(wine) <- c("Cultivar", "Alcohol", "Malic acid", "Ash",
                    "Alcalinity of ash", "Magnesium", "Total phenols",
                    "Flavanoids", "Nonflavanoid phenols", "Proanthocyanins",
                    "Color intensity", "Hue", "OD280/OD315 of diluted wines",
                    "Proline")

# The first column corresponds to the cultivar class
cultivar_classes <- factor(wine$Cultivar)

winePCA <- prcomp(scale(wine[,-1]))
plot(winePCA$x[,1:2], col = cultivar_classes, main="PCA test plot")
legend("bottomright", legend = c("Cv1","Cv2","Cv3"), pch = 1,
       col = c("black","red","green"))
```

---

### PCAs can take us further, but not in "plain" R

#### Introducing <!-- .element: class="fragment" data-fragment-index="1" --> 

[BioConductor](https://bioconductor.org/) <!-- .element: class="fragment" data-fragment-index="1" -->

|||

### When "vanilla" R just isn't enough

<ul>
<li class="fragment">One of the good things about R is it's expandability</li>
<li class="fragment">It is possible to "import" thousands of external 3rd party packages</li>
<li class="fragment">Bioconductor is more than just a package. It's a 3rd party package repository</li>
  <ul>
  <li class="fragment">It hosts ~~1473~~ ~~1649~~ 1823 bioinformatics related packages (at the time of writing)</li>
  <li class="fragment">It is very easy to use directly from R</li>
  </ul>
</ul>

---

### Trying out "pcaMethods" package

* Easy to use PCA extensions
* First we import the new package

```R
install.packages("BiocManager")
BiocManager::install("pcaMethods")
library(pcaMethods)
```

|||

### Trying out "pcaMethods" package

Next we calculate the PCA and plot it

```R
# Calculate PCA
winePCAmethods = pca(wine[,-1], scale="vector", center=T, nPcs=2, method="svd")

# Plot it
slplot(winePCAmethods,
       scol=cultivar_classes,
       scoresLoadings=c(TRUE,FALSE),
       spch=1)

legend("bottomright", legend = c("Cv1","Cv2","Cv3"), pch = 1,
       col = c("black","red","green"))

slplot(winePCAmethods,
       scoresLoadings=c(FALSE,TRUE))
```

|||

### Trying out "pcaMethods" package

Let's squeeze a little more information

```R
str(winePCAmethods) # slots are marked with @
winePCAmethods@R2
```

[Source](https://www.r-bloggers.com/principal-component-analysis-in-r/)

---

### Your turn!

* Can you perform a PCA of the `students.csv` dataset using "pcaMethods"?
* Try to obtain the % of explained variance for this dataset.

---

### Genomics data!

<ul>
  <li class="fragment">We will be using "real" data now</li>
  <li class="fragment">Obtained from RAD-Seq</li>
  <li class="fragment">Was already assembled, filtered and QC'ed for use</li>
  <li class="fragment">Is capped to 2000 SNPs and 26 individuals</li>
    <ul>
      <li class="fragment">Originally was 15322 SNPs and 46 individuals</li>
    </ul>
</ul>

<img src="assets/genome.jpg" style="background:none; border:none; box-shadow:none;" class="fragment">

---

### Now, for some genomics data

<ul>
  <li class="fragment">Data in [STRUCTURE](https://web.stanford.edu/group/pritchardlab/structure_software/release_versions/v2.3.4/structure_doc.pdf) format</li>
    <ul>
      <li class="fragment">A row with marker names</li>
      <li class="fragment">One row per individual with SNP data</li>
      <li class="fragment">"A", "C", "G" and "T" encoded as "1", "2", "3" and "4" respectively</li>
      <li class="fragment">Missing data encoded as "NA" or "-9"</li>
      <li class="fragment">All fields are space delimited</li>
    </ul>
  <li class="fragment">In this case, the first 4 columns are "Individual ID", "Population name", "Population number" and "Age"</li>
  <li class="fragment">Keep in mind that this format is **very** flexible</li>
</ul>

---

### First things first

* Load the data and take a look ate what we have

```R
library(pcaMethods)
snp_data = read.csv("https://stuntspt.gitlab.io/r_workshop_2020/assets/TLE.str",
                    header=TRUE,
                    row.names=1,
                    sep=" ")
View(snp_data)
```

|||

### "Calculate" the PCA and draw the plot using "Age" as the discriminant

```R
tlep_pca = pca(snp_data[,-c(1,2,3)],
               scale="none",
               center=T,
               nPcs=2,
               method="nipals")

# We have to use the `nipals` method because we have missing data
# We are also skipping columns 1 and 2 since the contain the population name and number respectively

print(tlep_pca@R2)

# Use the age group as discriminant
slplot(tlep_pca,
       scol=snp_data[, "Age"],
       scoresLoadings=c(TRUE,FALSE),
       sl=NULL,  # What does this do?
       spch="x")

legend("bottomright",
       legend=sort(unique(snp_data[, "Age"])),
       col=sort(unique(snp_data[, "Age"])),
       pch="x")
```

|||

### Now the same, but using "Population" as the discriminant

```R
# Use the population of origin as discriminant
slplot(tlep_pca,
       scol=snp_data[, "POP.NUMBER"],
       scoresLoadings=c(TRUE,FALSE),
       sl=NULL,  # What does this do?
       spch="x")

legend("bottomright",
       legend=unique(snp_data[, "POP.NAME"]),
       col=unique(snp_data[, "POP.NUMBER"]),
       pch="x")
```

---

# The end!

